
export interface IOptions {
    // globPattern?: string;
    command: string;
    checksumFile?: string;
    includes?: string[];
    excludes?: string[];
    outputFile?: string;
}

export interface IReplacement {
    search: string;
    replace: string;
}

export interface IInputReplacement {
    search?: string;
    searchRegExp?: string;
    replace?: string;
    replaceVar?: string;
    replaceEnv?: string;
}

