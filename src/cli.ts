import * as minimist from 'minimist';
// import * as fs from 'fs-extra';
import { IOptions } from './interfaces';
// import Checksummer from './Checksummer';
import runIfChanged from './runIfChanged';

let argv = minimist(process.argv.slice(2)) as any;



let options = {} as IOptions;

if (argv.include) {
    if (argv.include instanceof Array) {
        options.includes = argv.include;
    } else {
        options.includes = [argv.include];
    }
}

if (argv.exclude) {
    if (argv.exclude instanceof Array) {
        options.excludes = argv.exclude;
    } else {
        options.excludes = [argv.exclude];
    }
}


if (argv.checksumfile) {
    options.checksumFile = argv.checksumfile;
}

if (argv.o || argv.outputfile) {
    options.outputFile = argv.o || argv.outputfile;
}


let globPattern: string | undefined;

if (argv.f || argv.files) {
    globPattern = argv.f || argv.files;
}

if (argv.c || argv.command) {
    options.command = argv.c || argv.command;
}


let baseDir = process.cwd();
if (argv.basedir) {
    baseDir = argv.basedir;
}


if (argv.h || argv.help || !globPattern) {
    console.log('Usage: run-if-changed -f globPattern -c command [--checksumfile=.file.checksum] [--include=*.js] [--exclude=*.backup] [--outputfile=output.bin]');
    process.exit(0);
}



runIfChanged(globPattern || '', baseDir, options)
    .catch((error: any) => {
        console.error(error);
        process.exit(1);
    });
