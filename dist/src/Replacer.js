"use strict";
const fs = require('fs-extra');
const path = require('path');
const Promise = require('bluebird');
const glob = require('glob');
const underscore = require('underscore');
const child_process = require('child_process');
const minimatch = require('minimatch');
class Replacer {
    constructor(options) {
        if (options && options.includes) {
            this.includes = options.includes;
        }
        this.excludes = [];
        if (options && options.excludes) {
            this.excludes = options.excludes;
        }
        let ignoreFile = (options && options.excludeList) || path.join(__dirname, '/defaultignore');
        let ignores = fs.readFileSync(ignoreFile, 'utf-8').split('\n');
        this.excludes = this.excludes.concat(ignores);
        this.flags = 'g';
        if (options) {
            if (options.ignoreCase) {
                this.flags += 'i';
            }
            if (options.multiline) {
                this.flags += 'm';
            }
        }
    }
    replace(globPattern, folderPath, replacements, options) {
        return this.replaceFiles(globPattern, folderPath, this.parseReplacements(replacements), options);
    }
    replaceFiles(globPattern, folderPath, replacements, options) {
        let filenames = this.getFilenames(globPattern, folderPath);
        return Promise.map(filenames, (filename) => {
            let outputFilename = undefined;
            if (options && options.outputPath) {
                outputFilename = path.join(options.outputPath, filename.substring(folderPath.length + 1));
            }
            return this.replaceTextInFile(filename, replacements, outputFilename);
        });
    }
    replaceText(input, search, replacement) {
        return input.replace(new RegExp(search, this.flags), replacement);
    }
    matches(input, search) {
        return input.match(new RegExp(search, this.flags));
    }
    replaceTextInFile(filename, replacements, outputFilename) {
        return this.readFile(filename)
            .then((text) => {
            let replacedText = text;
            let hasMatch = false;
            replacements.forEach((replacement) => {
                let matches = this.matches(replacedText, replacement.search);
                if (matches && matches.length !== 0) {
                    hasMatch = true;
                    replacedText = this.replaceText(replacedText, replacement.search, replacement.replace);
                }
            });
            if (hasMatch === false) {
                return Promise.resolve(undefined);
            }
            if (outputFilename) {
                fs.ensureFileSync(outputFilename);
                return this.writeFile(outputFilename, replacedText);
            }
            return this.writeFile(filename, replacedText);
        });
    }
    getFilenames(globPattern, folderPath) {
        let result = [];
        result = glob.sync(globPattern, { cwd: folderPath });
        result = underscore.map(result, (filename) => path.join(folderPath, filename));
        result = underscore.filter(result, (filename) => {
            let fileStat = fs.statSync(filename);
            return fileStat.isSymbolicLink() === false && fileStat.isFile() && this.includeFile(filename, fileStat.isFile());
        });
        return result;
    }
    parseReplacements(replacements) {
        let result = [];
        replacements.forEach((replacement) => {
            result.push(this.parseReplacement(replacement));
        });
        return result;
    }
    includeFile(file, isFile) {
        let inIncludes = this.includes && this.includes.some((include) => {
            return minimatch(file, include, { matchBase: true });
        });
        let inExcludes = this.excludes.some((exclude) => {
            return minimatch(file, exclude, { matchBase: true });
        });
        return ((!this.includes || !isFile || inIncludes) && (!this.excludes || !inExcludes));
    }
    parseReplacement(inputReplacement) {
        let search = inputReplacement.search ? this.parseSearchString(inputReplacement.search) : inputReplacement.searchRegExp;
        let replace = inputReplacement.replace;
        if (inputReplacement.replaceEnv) {
            replace = process.env[inputReplacement.replaceEnv] ? this.parseSearchString(process.env[inputReplacement.replaceEnv]) : '';
        }
        else if (inputReplacement.replaceVar) {
            switch (inputReplacement.replaceVar) {
                case 'gitsha':
                    replace = this.parseSearchString(this.getGitSha());
                    break;
                default:
                    throw `Variable '${inputReplacement.replaceVar} is unknown`;
            }
        }
        return {
            search: search || '',
            replace: replace || ''
        };
    }
    getGitSha() {
        if (this.gitSha) {
            return this.gitSha;
        }
        this.gitSha = child_process.execSync('git rev-parse HEAD').toString().trim();
        return this.gitSha;
    }
    parseSearchString(search) {
        // http://stackoverflow.com/questions/1144783/replacing-all-occurrences-of-a-string-in-javascript
        return search.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
    }
    readFile(filename) {
        return new Promise((resolve, reject) => {
            fs.readFile(filename, 'utf-8', (error, result) => {
                if (error) {
                    return reject(error);
                }
                return resolve(result);
            });
        });
    }
    writeFile(filename, data) {
        return new Promise((resolve, reject) => {
            fs.writeFile(filename, data, (error) => {
                if (error) {
                    return reject(error);
                }
                return resolve({});
            });
        });
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Replacer;
//# sourceMappingURL=Replacer.js.map