import { IOptions } from './interfaces';
export declare const defaultChecksumFilename: string;
export declare function execshellPromise(command: string, options: any): Promise<void>;
export default function runIfChanged(globPattern: string, baseDir: string, options: IOptions): Promise<void>;
