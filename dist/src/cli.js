"use strict";
const minimist = require('minimist');
// import Checksummer from './Checksummer';
const runIfChanged_1 = require('./runIfChanged');
let argv = minimist(process.argv.slice(2));
let options = {};
if (argv.include) {
    if (argv.include instanceof Array) {
        options.includes = argv.include;
    }
    else {
        options.includes = [argv.include];
    }
}
if (argv.exclude) {
    if (argv.exclude instanceof Array) {
        options.excludes = argv.exclude;
    }
    else {
        options.excludes = [argv.exclude];
    }
}
if (argv.checksumfile) {
    options.checksumFile = argv.checksumfile;
}
if (argv.o || argv.outputfile) {
    options.outputFile = argv.o || argv.outputfile;
}
let globPattern;
if (argv.f || argv.files) {
    globPattern = argv.f || argv.files;
}
if (argv.c || argv.command) {
    options.command = argv.c || argv.command;
}
let baseDir = process.cwd();
if (argv.basedir) {
    baseDir = argv.basedir;
}
if (argv.h || argv.help || !globPattern) {
    console.log('Usage: run-if-changed -f globPattern -c command [--checksumfile=.file.checksum] [--include=*.js] [--exclude=*.backup] [--outputfile=output.bin]');
    process.exit(0);
}
runIfChanged_1.default(globPattern || '', baseDir, options)
    .catch((error) => {
    console.error(error);
    process.exit(1);
});
//# sourceMappingURL=cli.js.map