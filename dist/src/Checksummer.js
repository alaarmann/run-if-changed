"use strict";
const fs = require('fs-extra');
const path = require('path');
const Promise = require('bluebird');
const glob = require('glob');
const underscore = require('underscore');
// import * as child_process from 'child_process';
const md5File = require('md5-file');
const minimatch = require('minimatch');
const crypto = require('crypto');
class Checksummer {
    // private flags: string;
    constructor(options) {
        if (options && options.includes) {
            this.includes = options.includes;
        }
        this.excludes = [];
        if (options && options.excludes) {
            this.excludes = options.excludes;
        }
    }
    getMd5(globPattern, folderPath, options) {
        let filenames = this.getFilenames(globPattern, folderPath);
        // console.log('filename', filenames);
        return Promise
            .map(filenames, (filename) => {
            return md5File.sync(filename);
        })
            .then((hashes) => {
            const allHashes = hashes.join();
            if (allHashes === '') {
                return '';
            }
            return this.getMd5FromText(allHashes);
            // return Promise.resolve('');
        });
    }
    // public replace(globPattern: string, folderPath: string, replacements: IInputReplacement[], options?: IOptions): Promise<{}> {
    //     return this.replaceFiles(globPattern, folderPath, this.parseReplacements(replacements), options);
    // }
    // public replaceFiles(globPattern: string, folderPath: string, replacements: IReplacement[], options?: IOptions): Promise<{}> {
    //     let filenames = this.getFilenames(globPattern, folderPath);
    //     return Promise.map(filenames, (filename) => {
    //         let outputFilename = undefined as string | undefined;
    //         if (options && options.outputPath) {
    //             outputFilename = path.join(options.outputPath, filename.substring(folderPath.length + 1));
    //         }
    //         return this.replaceTextInFile(filename, replacements, outputFilename);
    //     });
    // }
    // public replaceText(input: string, search: string, replacement: string): string {
    //     return input.replace(new RegExp(search, this.flags), replacement);
    // }
    // public matches(input: string, search: string): RegExpMatchArray | /* tslint:disable */null/* tslint:enable */ {
    //     return input.match(new RegExp(search, this.flags));
    // }
    // public replaceTextInFile(filename: string, replacements: IReplacement[], outputFilename?: string): Promise<any> {
    //     return this.readFile(filename)
    //         .then((text) => {
    //             let replacedText = text;
    //             let hasMatch = false;
    //             replacements.forEach((replacement) => {
    //                 let matches = this.matches(replacedText, replacement.search);
    //                 if (matches && matches.length !== 0) {
    //                     hasMatch = true;
    //                     replacedText = this.replaceText(replacedText, replacement.search, replacement.replace);
    //                 }
    //             });
    //             if (hasMatch === false) {
    //                 return Promise.resolve(undefined);
    //             }
    //             if (outputFilename) {
    //                 fs.ensureFileSync(outputFilename);
    //                 return this.writeFile(outputFilename, replacedText);
    //             }
    //             return this.writeFile(filename, replacedText);
    //         });
    // }
    getFilenames(globPattern, folderPath) {
        let result = [];
        result = glob.sync(globPattern, { cwd: folderPath });
        result = underscore.map(result, (filename) => path.join(folderPath, filename));
        result = underscore.filter(result, (filename) => {
            let fileStat = fs.statSync(filename);
            return fileStat.isSymbolicLink() === false && fileStat.isFile() && this.includeFile(filename, fileStat.isFile());
        });
        return result;
    }
    // public parseReplacements(replacements: IInputReplacement[]): IReplacement[] {
    //     let result = [] as IReplacement[];
    //     replacements.forEach((replacement) => {
    //         result.push(this.parseReplacement(replacement));
    //     });
    //     return result;
    // }
    getMd5FromText(text) {
        return crypto.createHash('md5').update(text).digest('hex');
    }
    includeFile(file, isFile) {
        let inIncludes = this.includes && this.includes.some((include) => {
            return minimatch(file, include, { matchBase: true });
        });
        let inExcludes = this.excludes.some((exclude) => {
            return minimatch(file, exclude, { matchBase: true });
        });
        return ((!this.includes || !isFile || inIncludes) && (!this.excludes || !inExcludes));
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Checksummer;
//# sourceMappingURL=Checksummer.js.map