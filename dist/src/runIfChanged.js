"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const execshell = require('exec-sh');
const fs = require('fs');
const Checksummer_1 = require('./Checksummer');
const path = require('path');
exports.defaultChecksumFilename = '.runifchanged.checksum';
function execshellPromise(command, options) {
    let promise = new Promise((resolve, reject) => {
        execshell(command, options, (error, stdout, stderr) => {
            if (error) {
                return reject(error);
            }
            return resolve();
        });
    });
    return promise;
}
exports.execshellPromise = execshellPromise;
function runIfChanged(globPattern, baseDir, options) {
    return __awaiter(this, void 0, void 0, function* () {
        let checksumFilename = path.join(baseDir, options.checksumFile || exports.defaultChecksumFilename);
        let checksummer = new Checksummer_1.default(options);
        if (options.outputFile && fs.existsSync(options.outputFile) === false) {
            yield execshellPromise(options.command, undefined);
        }
        else {
            let filesChecksum = yield checksummer.getMd5(globPattern, baseDir, options);
            if (fs.existsSync(checksumFilename) === false) {
                yield execshellPromise(options.command, undefined);
            }
            else {
                let oldChecksum = fs.readFileSync(checksumFilename).toString();
                if (filesChecksum !== oldChecksum) {
                    yield execshellPromise(options.command, undefined);
                }
            }
            fs.writeFileSync(checksumFilename, filesChecksum);
        }
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = runIfChanged;
//# sourceMappingURL=runIfChanged.js.map